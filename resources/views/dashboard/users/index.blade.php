@extends('layouts.dashbord.master')

@section('content')
@if($Users->count() > 0)

<section class="content">

    <div class="row">
     <div class="col-xs-3">
        <a href="{{route('dashboard.users.create')}}" class="btn btn-primary">Create</a>
     </div>
    </div>
       <!-- /.row -->
   <div class="row">

     <div class="col-xs-12">
       <div class="box">
         <div class="box-header">
           <h3 class="box-title">users Table</h3>

         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive no-padding">
           <table class="table table-hover">
             <tr>
               <th>ID</th>
               <th>First-Name</th>
               <th>Last-Name</th>
               <th>Email</th>
               <th>Password</th>
               <th>Operation</th>
             </tr>
             @foreach ($Users as $user)


             <tr>
               <td>{{ $user->id }}</td>
               <td>{{ $user->first_name }}</td>
               <td>{{ $user->last_name }}</td>
               <td>{{ $user->email }}<td>
               <td>{{ $user->password }}<td>
               <td>
                       <a href="{{route('dashboard.users.edit',$user->id)}}" class="btn btn-success ">Edit</a>
                        <form method="POST" action="{{route('dashboard.users.destroy',$user->id)}}">
                           {{ csrf_field() }}
                             {{ method_field('DELETE') }}
                             <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure, You want to delete this Data?')">Delete</button>
                       </form> 
               </td>
             </tr>
             @endforeach
           </table>
        </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
     </div>
   </div>
 </section>

 
@else
<h2>@lang('site.no_data_found')</h2>

@endif

@endsection