@extends('layouts.dashbord.master')

@section('content')
<section class="content">
    <div class="row">

            <div class="col-md-1">
            </div>
      <!-- left column -->
      <div class="col-md-10">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Create New users</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form  action="#" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <div class="box-body">

              <div class="form-group">
                <label for="name">First Name</label>
                <input type="text" class="form-control" name="first_name" placeholder="Enter first_name ">
              </div>


              <div class="form-group">
                <label for="name">Last Name</label>
                <input type="text" class="form-control" name="last_name" placeholder="Enter last_name">
              </div>


              <div class="form-group">
                <label for="name">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Enter email">
              </div>


              <div class="form-group">
                <label for="name">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Enter Password">
              </div>

            <!-- /.box-body -->


                <button type="submit" class="btn btn-primary">Submit</button>

          </form>
        </div>
        <!-- /.box -->

        <div class="col-md-1">
        </div>

    </div>
    <!-- /.row -->
</section>

@endsection